import {combineReducers} from "redux";
import dataReducer from "./dataReducer";

const reducers = combineReducers({
    dataReducer: dataReducer
});

export default reducers;