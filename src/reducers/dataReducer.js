import {GET_DATA} from "../actions";

const INIT_STATE = {
  data: ''
};

export default function dataReducer(state=INIT_STATE, action) {
    switch (action.type) {
        case GET_DATA:
            return {
                ...state,
                data: action.payload
            };
        default:
            return INIT_STATE;
    }
}