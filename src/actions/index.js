export const GET_DATA = 'GET_DATA';

export const getData = () => ((dispatch) => {
    fetch('http://192.168.45.41:3000/test')
        .then(res => res.text())
        .then(data => dispatch({
            type: GET_DATA,
            payload: data
        }))
});