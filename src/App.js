import React, {Component} from 'react';
import {GET_DATA, getData} from "./actions";
import {connect} from "react-redux";

class App extends Component {

    componentDidMount() {
        this.props.getData();
    }

    render() {
        return (
            <div>
                {this.props.data}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    data: state.dataReducer.data
});

const mapDispatchToProps = dispatch => ({
    getData: () => dispatch(getData())
});


export default connect(mapStateToProps,mapDispatchToProps)(App);